%global pkg_name %{name}
%{?java_common_find_provides_and_requires}
Name:		mongo-java-driver
Version:	3.6.4
Release:	1
Summary:	A Java driver for MongoDB
BuildArch:	noarch
License:	ASL 2.0
URL:		http://www.mongodb.org/display/DOCS/Java+Language+Center
Source0:	https://github.com/mongodb/mongo-java-driver/archive/r%{version}.tar.gz
Patch0:         mongo-java-driver-gradle-local-fixes.patch
BuildRequires:       java-devel
BuildRequires:       gradle-local javapackages-tools javapackages-local mvn(io.netty:netty-buffer)
BuildRequires:       mvn(io.netty:netty-transport) mvn(io.netty:netty-handler)
BuildRequires:       mvn(org.slf4j:slf4j-api) mvn(org.xerial.snappy:snappy-java)
Requires:            javapackages-tools
%description
This is an ueber jar for the MongoDB Java driver.

%package bson
Summary:	A Java-based BSON implementation
Requires:            javapackages-tools
%description bson
This is the Java implementation of BSON that the Java driver for
MongoDB ships with.  It can be used separately by Java applications
that require BSON.

%package driver
Summary:	The MongoDB Java Driver
Requires:            javapackages-tools
%description driver
The MongoDB Java Driver

%package driver-core
Summary:	The MongoDB Java Operations Layer
Requires:            javapackages-tools
%description driver-core
The Java operations layer for the MongoDB Java Driver. Third
parties can wrap this layer to provide custom higher-level APIs

%package driver-async
Summary:	The MongoDB Java Async Driver
Requires:            javapackages-tools
%description driver-async
The MongoDB Asynchronous Driver.

%prep
%setup -qn %{pkg_name}-r%{version}
%patch0 -p1
find -name '*.class' -exec rm -f '{}' \;
find -name '*.jar' -exec rm -f '{}' \;
set -ex
%mvn_package org.mongodb:bson:* %{pkg_name}-bson
%mvn_package org.mongodb:%{pkg_name}:* %{pkg_name}
%mvn_package org.mongodb:mongodb-driver-core:* %{pkg_name}-driver-core
%mvn_package org.mongodb:mongodb-driver-async:* %{pkg_name}-driver-async
%mvn_package org.mongodb:mongodb-driver:* %{pkg_name}-driver
%mvn_package org.mongodb:mongodb-javadoc-utils:* __noinstall
%mvn_file org.mongodb:bson:* %{pkg_name}/bson
%mvn_file org.mongodb:%{pkg_name}:* %{pkg_name}/mongo
%mvn_file org.mongodb:mongodb-driver-core:* %{pkg_name}/driver-core
%mvn_file org.mongodb:mongodb-driver-async:* %{pkg_name}/driver-async
%mvn_file org.mongodb:mongodb-driver:* %{pkg_name}/driver

%build
%gradle_build -f -- -s -i

%install
%mvn_install

%files -f .mfiles-%{pkg_name}
%doc README.md LICENSE.txt

%files bson -f .mfiles-%{pkg_name}-bson
%doc README.md LICENSE.txt

%files driver -f .mfiles-%{pkg_name}-driver
%doc README.md LICENSE.txt

%files driver-core -f .mfiles-%{pkg_name}-driver-core
%doc README.md LICENSE.txt

%files driver-async -f .mfiles-%{pkg_name}-driver-async
%doc README.md LICENSE.txt

%changelog
* Mon Aug 17 2020 Shaoqiang Kang <kangshaoqiang1@huawei.com> - 3.6.4-1
- Package init
